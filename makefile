#CC=arm-linux-gcc
#CXX=g++
#AR=ar

COMPILER_VERSION        = i386-linux-gnu

## CFLAGS
CFLAGS_APP              = -DBTEXT -D_LINUX -Os
CFLAGS_DEBUG            = -DDEBUG -g3


APP_LINKFLAGS			+= `pkg-config --libs wayland-client`

# Source code path for lib
CLIENT_SOURCE = \
	simple-touch.c \
	./shared/os-compatibility.c \

INCLUDE_WAYLAND_SHARED = -I. \
	-I./shared/ \

release :
	mkdir -p wayland_sample
	$(CC) $(INCLUDE_WAYLAND_SHARED) $(APP_LINKFLAGS) -DDEBUG -g -o simpletouch $(CLIENT_SOURCE)
	chmod 755 simpletouch